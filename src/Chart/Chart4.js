import React, { Component } from 'react'   
import{Pie}from'@ant-design/charts'; 

class App extends Component {
    state = {
      status: 1
    };
    render() {
        var data = [ 
            {
              type : 'Classification One' , 
              value:27, 
            },
            {
              type : 'Class 2' , 
              value:25, 
            },
            {
              type : 'Classification Three' , 
              value:18, 
            },
            {
              type : 'Classification Four' , 
              value:15, 
            },
            {
              type : 'Classification Five' , 
              value:10, 
            },
            {
              type : 'Other' , 
              value:5, 
            },
          ];
          var config ={ 
            appendPadding:10, 
            data : data,
            angleField:'value', 
            colorField:'type', 
            width:500,
            height:500,
            radius:0.8, 
            label:{ 
              type:'inner', 
              offset:'-0.5', 
              content:'{name} {percentage}', 
              style:{ 
                fill:'#fff', 
                fontSize:14, 
                textAlign : 'center' , 
              },
            },
            interactions:[{type:'element-active'}],    
          };
          
          return (
            <>
            <Pie {...config}/>
             </>
             );
            }
          }
          
          export default App;