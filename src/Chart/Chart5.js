import React, { Component } from 'react'   
import{RingProgress}from'@ant-design/charts'; 

class App extends Component {
    state = {
      status: 1
    };
    render() {
      
            var config ={ 
              height: 100,
              width: 100,
              autoFit: false,
              percent: 0.6,
              color: ['#F4664A', '#E8EDF3'],
              innerRadius: 0.85,
              radius: 0.98,
            };
          
          return (
            <>
            <RingProgress {...config}/>
             </>
             );
            }
          }
          
          export default App;