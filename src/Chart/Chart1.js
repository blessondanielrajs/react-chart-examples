import React, { Component } from 'react'   
import{Line}from'@ant-design/charts';  



class App extends Component {
    state = {
      status: 1
    };
    render() {
        var data = [ 
            {
              year:'1991', 
              value:3, 
            },
            {
              year:'1992', 
              value:4, 
            },
            {
              year:'1993', 
              value:3.5, 
            },
            {
              year:'1994', 
              value:5, 
            },
            {
              year:'1995', 
              value:4.9, 
            },
            {
              year:'1996', 
              value:6, 
            },
            {
              year:'1997', 
              value:7, 
            },
            {
              year:'1998', 
              value:9, 
            },
            {
              year:'1999', 
              value:13, 
            },
          ];
          var config ={ 
            data : data ,
            xField:'year', 
            yField:'value', 
            width:500,
            height:500,
            label:{}, 
            point:{ 
              size:5, 
              shape:'diamond', 
              style: {
                fill: 'red',
                fillOpacity: 0.5,
                stroke: 'black',
                lineWidth: 1,
                lineDash : [ 4 , 5 ] ,  
                strokeOpacity: 0.7,
                shadowColor: 'black',
                shadowBlur: 10,
                shadowOffsetX: 5,
                shadowOffsetY: 5,
                cursor: 'pointer'
              }
            },
          };

        return (
          <>
          <Line {...config}/>
           </>
    );
  }
}

export default App;