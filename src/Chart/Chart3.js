import React, { Component } from 'react'   
import{Column}from'@ant-design/charts'; 

class App extends Component {
    state = {
      status: 1
    };
    render() {
        var data = [ 
            {
              type : 'furniture appliances' , 
              sales:38, 
            },
            {
              type : 'Grain, oil and non-staple food' , 
              sales:52, 
            },
            {
              type : 'Fresh fruit' , 
              sales:61, 
            },
            {
              type : 'Beauty Care' , 
              sales:145, 
            },
            {
              type : 'Mother and baby supplies' , 
              sales:48, 
            },
            {
              type : 'Imported Food' , 
              sales:38, 
            },
            {
              type : 'Food and Beverage' , 
              sales:38, 
            },
            {
              type : 'Household cleaning' , 
              sales:38, 
            },
          ];
          var config ={ 
            data : data ,
            xField:'type', 
            yField:'sales', 
            width:500,
            height:500,
            seriesField: '',
            color: function color(_ref) {
              var type = _ref.type;
              return type === 'Beauty Care' ? '#FAAD14' : '#5B8FF9' ;       
            },
            legend: false,
            meta:{ 
              type : { alias : 'category' } ,   
              sales : { alias : 'sales' } ,   
            },
          };
          
          return (
            <>
            <Column {...config}/>
             </>
             );
            }
          }
          
          export default App;