import React, { Component } from 'react'
import { Card,Select } from 'antd';
import './App.less';
import Chart1 from './Chart/Chart1';
import Chart2 from './Chart/Chart2';
import Chart3 from './Chart/Chart3';
import Chart4 from './Chart/Chart4';
import Chart5 from './Chart/Chart5';
const { Meta } = Card;
const { Option } = Select;


class App extends Component {
  state = {
    IMAGE: [1]
  };

  handleChange = (value) => {
    this.setState({ IMAGE: value })
    console.log(value);
    console.log(this.state.IMAGE);

}


  render() {


    return (
      <>
       

<Select mode="tags" style={{ width: '100%' }} placeholder="Tags Mode" onChange={this.handleChange}>
    <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
  </Select>
  
   

   
        {
          this.state.IMAGE.map(
            (item, i)=> {
            return (
           

            <Card
              hoverable
              style={{ width: 240 }}
              cover={<img src={require('./images/' + item + '.jpeg')} />}
            >
              <Meta title="Europe Street beat" description="www.instagram.com" />
            </Card>
            )
          }
            )          
        }
    
          
  
            
           
      <Chart1/>
      <Chart2/>
      <Chart3/>
      <Chart4/>
      <Chart5/>
      </>
    );
  }
}

export default App;